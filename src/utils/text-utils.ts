/*
 Зависимых форм немного, склоняем руками
 */

export const getCommentsForm = (num: number) => {
  const lastDigit = num % 10;

  if (lastDigit > 1 && lastDigit < 5) {
    return 'комментария';
  }

  if (lastDigit > 4 || lastDigit === 0 || (num > 10 && num < 20)) {
    return 'комментариев';
  }

  return 'комментарий';
};

export const getNewForm = (num: number) => {
  const lastDigit = num % 10;

  if (lastDigit === 1) {
    return 'новый';
  }

  return 'новых';
};
