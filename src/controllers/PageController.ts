/*
  Так как логики немного, вся бизнес-логика и все обращения к сервисам аккумулированы в это коонтроллере.
  Остальные контроллеры отвечают лишь за рендеринг передаваемых им данных.

  Если нужно открыть новый пост - создаем новый экземпляр контроллера, предварительно вызвав dispose,
  либо вызываем dispose и init методы в этом контроллере
 */
import {comments as commentsService} from '../services';
import {Comment} from '../models';
import {CommentsController} from './Comments';
import {CommentsNumberController} from './CommentsNumber';
import {NewCommentsController} from './NewComments';
import {AxiosPromise} from 'axios';

export class PageController {
  private comments: Comment[] = [];
  private newComments: Comment[] = []; // комментарии, еще не добавленные в список
  private commentsController: CommentsController;
  private commentsNumberController: CommentsNumberController;
  private newCommentsController: NewCommentsController;

  constructor(private postId: number | string, private root: HTMLElement) {
    this.setNewComment = this.setNewComment.bind(this);
    this.resetNewComments = this.resetNewComments.bind(this);
    this.createComment = this.createComment.bind(this);

    this.init();
  }

  async init() {
    try {
      this.comments = (await this.getComments()).data;
      commentsService.subscribe(this.setNewComment);
    } catch (e) {
      this.renderError();
    }

    this.commentsNumberController = new CommentsNumberController(this.root, this.comments.length);
    this.commentsController = new CommentsController(this.root, this.comments);
    this.newCommentsController = new NewCommentsController(this.root, this.newComments.length);

    const el = this.root.getElementsByClassName('comments__new')[0] as HTMLElement;
    el.onclick = this.resetNewComments;

    const form = document.getElementById('form');
    form.onsubmit = this.createComment;
  }

  getComments(): AxiosPromise<Comment[]> {
    return commentsService.getByPostId(this.postId);
  }

  setNewComment(comment: Comment) {
    this.newComments.push(comment);
    this.commentsNumberController.setCommentsNumber(this.comments.length + this.newComments.length);
    this.newCommentsController.setCommentsNumber(this.newComments.length);
  }

  // открывем новые коментарии
  resetNewComments() {
    this.newCommentsController.setCommentsNumber(0);
    this.commentsController.addComments(this.newComments);
    this.comments = [...this.comments, ...this.newComments];
    this.newComments = [];
  }

  async createComment(e: Event) {
    e.preventDefault();
    const input = document.getElementById('comment-input') as HTMLInputElement;
    if (!input || !input.value) {
      return;
    }

    const comment: Comment = {
      id: Date.now(),
      postId: this.postId,
      name: 'Пользователь',
      email: 'user@fake.fake',
      body: input.value,
    };

    try {
      await commentsService.create(comment);
      this.comments.push(comment);
      this.commentsNumberController.setCommentsNumber(this.comments.length + this.newComments.length);
      this.commentsController.addComment(comment);
      input.value = '';
    } catch (e) {
      alert('Не получилось добавить комментарий');
    }
  }

  renderError() {
    this.dispose();
    alert('Не получилось загрузить комментарии');
    this.init();
  }

  dispose() {
    this.root.innerHTML = '';
    this.comments = [];
    this.newComments = [];
    commentsService.unsunscribe(this.setNewComment);

    const el = this.root.getElementsByClassName('comments__new')[0] as HTMLElement;
    el && (el.onclick = () => false);

    const form = document.getElementById('form');
    form && (form.onsubmit = () => false);
  }
}