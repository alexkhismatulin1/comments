import {Comment} from '../models';
import {CommentController} from './Comment';

export class CommentsController {
  private root = document.createElement('div');

  constructor(parent: HTMLElement, comments: Comment[] = []) {
    this.root.className = 'comments__list';
    parent.appendChild(this.root);

    this.addComments(comments);
  }

  addComments(comments: Comment[]) {
    comments.forEach((comment) => this.addComment(comment));
  }

  addComment(comment: Comment) {
    const ctrl = new CommentController(comment);
    this.root.appendChild(ctrl.render());
  }
}