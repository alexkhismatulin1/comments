import {getCommentsForm} from '../utils';

export class CommentsNumberController {
  private root = document.createElement('div');

  constructor(parent: HTMLElement, _number: number) {
    this.root.className = 'comments__number';
    parent.appendChild(this.root);

    Number.isInteger(_number) && this.setCommentsNumber(_number);
  }

  setCommentsNumber(num: number) {
    this.root.innerText = `${num} ${getCommentsForm(num)}`;
  }
}