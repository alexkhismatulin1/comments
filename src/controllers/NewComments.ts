import {getNewForm, getCommentsForm} from '../utils';

export class NewCommentsController {
  private root = document.createElement('div');

  constructor(parent: HTMLElement, _number?: number) {
    this.root.className = 'comments__new';
    parent.appendChild(this.root);

    this.setCommentsNumber(_number);
  }

  setCommentsNumber(num: number) {
    if (!num) {
      this.root.style.display = 'none';
      return;
    }

    this.root.style.display = 'flex';
    this.root.innerHTML = `<span>${num} ${getNewForm(num)} ${getCommentsForm(num)}</span>`;
  }
}