import {Comment} from '../models';

export class CommentController {
  template: string;

  constructor(private comment: Comment) {
    this.template = `
      <div class="comment__item">
          <div class="comment__item__header">
              <img src="https://via.placeholder.com/32x32" alt="avatar" />
              <p>${comment.email}</p>
          </div>
          <div class="comment__item__text">${comment.body}</div>
      </div>`;
  }

  render(): Node {
    const temp = document.createElement('div');
    temp.innerHTML = this.template;
    return temp.firstElementChild;
  }
}