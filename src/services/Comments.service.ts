import axios, {AxiosPromise} from 'axios';

import {apiUrl} from '../constants';
import {Comment} from '../models';
import {fakeSocket} from './FakeSocket.service';

class CommentsService {
  private readonly frequency = 5000;
  private readonly eventName = 'new_message';

  constructor() {
    const comment: Comment = {
      postId: 1,
      id: Date.now(),
      name: 'Александр Хисматулин',
      email: 'fake@fake.fake',
      body: 'Они все одинаковые. Все до единого. Приятного просмотра.',
    };

    setInterval(() => fakeSocket.emit(this.eventName, comment), this.frequency);
  }

  getAll(): AxiosPromise<Array<Comment>> {
    return axios.get(`${apiUrl}/comments`);
  }

  getByPostId(id: string | number): AxiosPromise<Array<Comment>> {
    return axios.get(`${apiUrl}/posts/${id}/comments`);
  }

  getById(id: string | number): AxiosPromise<Comment> {
    return axios.get(`${apiUrl}/comments/${id}`);
  }

  create(comment: Comment): Promise<void> {
    return Promise.resolve();
  }

  subscribe(cb: any) {
    fakeSocket.subscribe(this.eventName, cb);
  }

  unsunscribe(cb: any) {
    fakeSocket.unsubscribe(this.eventName, cb);
  }
}

export const comments = new CommentsService();
