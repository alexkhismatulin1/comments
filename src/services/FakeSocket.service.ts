class FakeSocket {
  private readonly events: { [key: string]: Array<any> } = {};

  subscribe(eventName: string, cb: any) {
    if (!this.events[eventName]) {
      this.events[eventName] = [];
    }

    this.events[eventName].push(cb);
  }

  unsubscribe(eventName: string, cb: any) {
    if (!this.events[eventName]) {
      return;
    }

    this.events[eventName] = this.events[eventName].filter((_cb) => _cb !== cb);
  }

  emit(eventName: string, data: any) {
    if (!this.events[eventName]) {
      return;
    }

    this.events[eventName].forEach((cb) => cb(data));
  }
}

export const fakeSocket = new FakeSocket();
